# Basic Emulab profile Ansible bootstrap shell library

This repository is intended to be included as a submodule into an
Ansible-based profile (e.g. in `emulab-ansible-bootstrap/`).  It assumes
that one node in your experiment will act as the configuration manager or
"head" node, and others will be configured from that node.  The "head" node
should add the `emulab-ansible-bootstrap/head.sh` script as its startup
script, and managed clients should add the
`emulab-ansible-bootstrap/client.sh` script.
